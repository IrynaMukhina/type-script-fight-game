export interface ICreateElement {
    tagName: string,
    className?: string,
    attributes?: any,
}
