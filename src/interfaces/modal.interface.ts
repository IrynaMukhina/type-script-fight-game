import { ICallBack } from "./index";

export interface IModal {
    title: string,
    bodyElement: string,
    onClose: ICallBack,
}
