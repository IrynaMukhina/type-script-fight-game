export interface IControls {
    PlayerOneAttack: string,
    PlayerOneBlock: string,
    PlayerTwoAttack: string,
    PlayerTwoBlock: string,
    PlayerOneCriticalHitCombination: string[],
    PlayerTwoCriticalHitCombination: string[],
}
