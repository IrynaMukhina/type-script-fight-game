export interface ISelectFighterCallBack {
    (event: Event, fighterId: string): void
}
