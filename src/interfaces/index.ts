export * from "./controls.interface";
export * from "./modal.interface";
export * from "./call-back.interface";
export * from "./fighter.interface";
export * from "./create-element.interface";
export * from "./select-fighter-call-back.interface";
