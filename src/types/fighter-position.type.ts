import { PositionEnum } from "../enums/index";

export type FighterPosition = PositionEnum.LEFT | PositionEnum.RIGHT;
