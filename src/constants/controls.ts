import { IControls } from "../interfaces/index";
import { KeysEnum } from "../enums/index";

export const controls: IControls = {
  PlayerOneAttack: KeysEnum.KEY_A,
  PlayerOneBlock: KeysEnum.KEY_D,
  PlayerTwoAttack: KeysEnum.KEY_J,
  PlayerTwoBlock: KeysEnum.KEY_L,
  PlayerOneCriticalHitCombination: [KeysEnum.KEY_Q, KeysEnum.KEY_W, KeysEnum.KEY_E],
  PlayerTwoCriticalHitCombination: [KeysEnum.KEY_U, KeysEnum.KEY_I, KeysEnum.KEY_O],
}
