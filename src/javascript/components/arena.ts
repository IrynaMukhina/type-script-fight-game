import { createElement } from '../helpers/domHelper';
import { createFighterImage } from './fighterPreview';
import { fight } from './fight';
import { showWinnerModal } from './modal/winner';
import { IFigther } from '../../interfaces/index';
import { PositionEnum } from '../../enums/index';
import { FighterPosition } from '../../types/index';

export async function renderArena(selectedFighters: IFigther[]): Promise<void> {
  const root = document.getElementById('root');
  const arena = createArena(selectedFighters);

  root!.innerHTML = '';
  root!.append(arena);

  try {
    const [playerOne, playerTwo] = selectedFighters;
    const winner = await fight(playerOne, playerTwo);

    showWinnerModal(winner);
  } catch (error) {
    throw error;
  }
}

function createArena(selectedFighters: IFigther[]): HTMLElement {
  const arena = createElement({ tagName: 'div', className: 'arena___root' });
  const [playerOne, playerTwo] = selectedFighters;

  const healthIndicators = createHealthIndicators(playerOne, playerTwo);
  const fighters = createFighters(playerOne, playerTwo);
  
  arena.append(healthIndicators, fighters);
  return arena;
}

function createHealthIndicators(leftFighter: IFigther, rightFighter: IFigther): HTMLElement {
  const healthIndicators = createElement({ tagName: 'div', className: 'arena___fight-status' });
  const versusSign = createElement({ tagName: 'div', className: 'arena___versus-sign' });
  const leftFighterIndicator = createHealthIndicator(leftFighter, PositionEnum.LEFT);
  const rightFighterIndicator = createHealthIndicator(rightFighter, PositionEnum.RIGHT);

  healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
  return healthIndicators;
}

function createHealthIndicator(fighter: IFigther, position: FighterPosition): HTMLElement {
  const { name } = fighter;
  const container = createElement({ tagName: 'div', className: 'arena___fighter-indicator' });
  const fighterName = createElement({ tagName: 'span', className: 'arena___fighter-name' });
  const indicator = createElement({ tagName: 'div', className: 'arena___health-indicator' });
  const bar = createElement({ tagName: 'div', className: 'arena___health-bar', attributes: { id: `${position}-fighter-indicator` }});

  fighterName.innerText = name;
  indicator.append(bar);
  container.append(fighterName, indicator);

  return container;
}

function createFighters(firstFighter: IFigther, secondFighter: IFigther): HTMLElement {
  const battleField = createElement({ tagName: 'div', className: `arena___battlefield` });
  const firstFighterElement = createFighter(firstFighter, PositionEnum.LEFT);
  const secondFighterElement = createFighter(secondFighter, PositionEnum.RIGHT);

  battleField.append(firstFighterElement, secondFighterElement);
  return battleField;
}

function createFighter(fighter: IFigther, position: FighterPosition): HTMLElement {
  const imgElement = createFighterImage(fighter);
  const positionClassName = position === PositionEnum.RIGHT ? 'arena___right-fighter' : 'arena___left-fighter';
  const fighterElement = createElement({
    tagName: 'div',
    className: `arena___fighter ${positionClassName}`,
  });

  fighterElement.append(imgElement);
  return fighterElement;
}
