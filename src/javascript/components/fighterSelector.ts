import { createElement } from '../helpers/domHelper';
import { renderArena } from './arena';
import { createFighterPreview } from './fighterPreview';
import { fighterService } from '../services/fightersService';
import { IFigther } from '../../interfaces/index';
import { PositionEnum } from '../../enums/index';

export function createFightersSelector() {
  let selectedFighters: IFigther[] = [];

  return async (_: Event, fighterId: string) => {
    const fighter = await getFighterInfo(fighterId);
    const [playerOne, playerTwo] = selectedFighters;
    const firstFighter = playerOne ?? fighter;
    const secondFighter = Boolean(playerOne) ? playerTwo ?? fighter : playerTwo;
    selectedFighters = [firstFighter, secondFighter];

    renderSelectedFighters(selectedFighters);
  };
}

const fighterDetailsMap = new Map();

export async function getFighterInfo(fighterId: string): Promise<IFigther> {
  if (fighterDetailsMap.has(fighterId)) {
    return fighterDetailsMap.get(fighterId);
  }

  try {
    const fighterInfo: IFigther = await fighterService.getFighterDetails(fighterId);

    fighterDetailsMap.set(fighterId, fighterInfo);

    return fighterInfo;
  } catch (error) {
    throw error;
  }
}

function renderSelectedFighters(selectedFighters: IFigther[]): void {
  const fightersPreview = document.querySelector('.preview-container___root');
  const [playerOne, playerTwo] = selectedFighters;
  const firstPreview = createFighterPreview(playerOne, PositionEnum.LEFT);
  const secondPreview = createFighterPreview(playerTwo, PositionEnum.RIGHT);
  const versusBlock = createVersusBlock(selectedFighters);

  fightersPreview!.innerHTML = '';
  fightersPreview!.append(firstPreview, versusBlock, secondPreview);
}

function createVersusBlock(selectedFighters: IFigther[]): HTMLElement {
  const canStartFight = selectedFighters.filter(Boolean).length === 2;
  const onClick = () => startFight(selectedFighters);
  const container = createElement({ tagName: 'div', className: 'preview-container___versus-block' });
  const image = createElement({
    tagName: 'img',
    className: 'preview-container___versus-img',
    attributes: { src: '../../../resources/versus.png' },
  });
  const disabledBtn = canStartFight ? '' : 'disabled';
  const fightBtn = createElement({
    tagName: 'button',
    className: `preview-container___fight-btn ${disabledBtn}`,
  });

  fightBtn.addEventListener('click', onClick, false);
  fightBtn.innerText = 'Fight';
  container.append(image, fightBtn);

  return container;
}

function startFight(selectedFighters: IFigther[]): void {
  renderArena(selectedFighters);
}
