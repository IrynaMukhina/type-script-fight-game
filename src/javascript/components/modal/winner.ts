import { showModal } from './modal';
import { IFigther } from '../../../interfaces/index';

export function showWinnerModal(fighter: IFigther): void {
  showModal({
    title: 'Game Over.',
    bodyElement: `Winer is ${fighter.name}`,
    onClose: () => console.log('The End')
  });
}
