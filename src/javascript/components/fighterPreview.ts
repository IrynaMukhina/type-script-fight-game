import { createElement } from '../helpers/domHelper';
import { IFigther } from '../../interfaces/index';
import { FighterPosition } from '../../types/index';

export function createFighterPreview(fighter: IFigther, position: FighterPosition): HTMLElement {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (fighter) {
    const fighterImg = createFighterPreviewImage(fighter);

    const fighterName = createElement({
      tagName: 'p',
      className: 'fighter-preview___info',
    });
    fighterName.innerHTML = `Name: ${fighter.name}`;

    const fighterHealth = createElement({
      tagName: 'p',
      className: 'fighter-preview___info',
    });
    fighterHealth.innerHTML = `Health: ${fighter.health}`;

    const fighterAttack = createElement({
      tagName: 'p',
      className: 'fighter-preview___info',
    });
    fighterAttack.innerHTML = `Attack: ${fighter.attack}`;

    const fighterDefense = createElement({
      tagName: 'p',
      className: 'fighter-preview___info',
    });
    fighterDefense.innerHTML = `Defense: ${fighter.defense}`;
  
    fighterElement.appendChild(fighterImg);
    fighterElement.appendChild(fighterName);
    fighterElement.appendChild(fighterHealth);
    fighterElement.appendChild(fighterAttack);
    fighterElement.appendChild(fighterDefense);
  }
  
  return fighterElement;
}

export function createFighterImage(fighter: IFigther): HTMLElement {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

function createFighterPreviewImage(fighter: IFigther): HTMLElement {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img--small',
    attributes,
  });

  return imgElement;
}
