export enum KeysEnum {
    KEY_A = 'KeyA',
    KEY_D = 'KeyD',
    KEY_J = 'KeyJ',
    KEY_L = 'KeyL',
    KEY_Q = 'KeyQ',
    KEY_E = 'KeyE',
    KEY_W = 'KeyW',
    KEY_U = 'KeyU',
    KEY_I = 'KeyI',
    KEY_O = 'KeyO',
}
